package com.zuitt.batch193;

import java.util.Scanner;

public class activity {
    public static void main(String[] args){
        Scanner studentScanner = new Scanner(System.in);

        System.out.println("First Name:");
        String firstName = studentScanner.nextLine().trim();

        System.out.println("Last Name:");
        String lastName = studentScanner.nextLine().trim();

        System.out.println("First Subject Grade");
        double firstSubjectGrade = studentScanner.nextDouble();

        System.out.println("Second Subject Grade");
        double secondSubjectGrade = studentScanner.nextDouble();

        System.out.println("Third Subject Grade");
        double thirdSubjectGrade = studentScanner.nextDouble();

        int average = (int) (firstSubjectGrade + secondSubjectGrade + thirdSubjectGrade) / 3;

        System.out.println("Good Day, " + firstName + " " + lastName + ".");

        System.out.println("Your grade average is: " + average);
    }
}
