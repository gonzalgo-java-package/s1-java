package com.zuitt.batch193;

import java.util.Arrays;

public class HelloWorld {

//public = access modifiers - who can see this method
//static = non-access modifier - how should this method behave
    //static keyword field exists across all the class instances
//void = return type - what should this method return
    //void keyword specify that a method doesn't return anything
//main = method name
    public static void main(String[] args) {
        System.out.println("Hello World");

    }

}
