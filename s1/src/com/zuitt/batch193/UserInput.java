package com.zuitt.batch193;

import java.util.Scanner;

public class UserInput {
    public static void main(String[] args) {
        //scanner class is used to get your user input and is imported from java.util package
        //Scanner is an object to gather user input
        Scanner appScanner = new Scanner(System.in);
        System.out.println("What's your name?");
        String myName = appScanner.nextLine().trim();
        //nextline() - will always convert the value to an object
        //trim() - removes white spaces before and after the words
        System.out.println("Username is: " + myName);

        System.out.println("What's your age?");
        int myAge = appScanner.nextInt();
        System.out.println("Age of the user is " + myAge);

        System.out.println("What is your weight?");
        double weight = appScanner.nextDouble();
        System.out.println("The weight of the user is: " + weight);
    }
}
